# MyProfileApi

## REQUIREMENTS
------------

- PHP >= 5.6.4
- Yii >= 5.4.*
- Docker Composer >=  1.9.0
- Composer >= 1.5.*

## BUILD
-------

To run the project, open tour terminal and run:

```
docker-compose build
```

## RUN
-------

to run the app you must run in your terminal

```
docker-compose up
```

and access: localhost:8080

## CONFIGURE API
-------

You need configure github user to make requests. For this, go to `app/GithubUser` and
change `githubAuth` attribute with github username and password.