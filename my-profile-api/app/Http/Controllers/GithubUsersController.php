<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\GithubUser;

/**
 * Control requests to github api
 * 
 * @author Paulo Henrique <paullo_h@hotmail.com>
 */
class GithubUsersController extends Controller
{
    /**
     * Get Github user by username
     *
     * @param string $username
     * @return GithubUser
     */
    public function index($username)
    {
        return (new GithubUser($username))->getUser();
    }

    /**
     * Get all followers of a user
     *
     * @param string $username
     * @param int $page
     * @return array GithubUsers
     */
    public function findFollowers($username, $page)
    {
        return (new GithubUser($username, $page))->getFollowers();
    }

    /**
     * Get all following users
     *
     * @param string $username
     * @param int $page
     * @return array GithubUsers
     */
    public function findFollowing($username, $page)
    {
        return (new GithubUser($username, $page))->getFollowing();
    }

    /**
     * Get all orgs of a user
     *
     * @param string $username
     * @return array
     */
    public function findOrgs($username)
    {
        return (new GithubUser($username))->getOrgs();
    }

    /**
     * Get all repos of a user
     *
     * @param string $username
     * @param int $page
     * @return array
     */
    public function findRepos($username, $page)
    {
        return (new GithubUser($username, $page))->getRepos();
    }
}
