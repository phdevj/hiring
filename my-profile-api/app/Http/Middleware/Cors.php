<?php

namespace App\Http\Middleware;

use Closure;

/**
 * Configure CORS
 * 
 * @author Paulo Henrique <paullo_h@hotmail.com>
 */
class Cors
{
    /**
     * Handle an incoming request.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request)
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, OPTIONS');
    }
}
