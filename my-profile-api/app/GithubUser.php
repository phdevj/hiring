<?php

namespace App;

/**
 * Make request to github api
 * 
 * @author Paulo Henrique <paullo_h@hotmail.com>
 */
class GithubUser
{
    private $username;
    private $page;
    private $githubAuth;
    
    function __construct($username, $page = 1)
    {
        $this->username = $username;
        $this->page = $page;
        /**
         * Configure github user for incrense 
         * request limit to 5000/per hour
         * user/password
         */
        $this->githubAuth = ['username', 'password'];
    }

    public function getUser()
    {
        $user = $this->getUrl('https://api.github.com/users/' . $this->username);

        if (!$user) {
            return false;
        }

        return $user;
    }

    public function getFollowers()
    {
        return $this->getUrl(
            'https://api.github.com/users/' . $this->username . '/followers?page=' . $this->page
        );
    }
    
    public function getFollowing()
    {
        return $this->getUrl(
            'https://api.github.com/users/' . $this->username . '/following?page=' . $this->page
        );
    }

    public function getOrgs()
    {
        return $this->getUrl(
            'https://api.github.com/users/' . $this->username . '/orgs?per_page=100'
        );
    }

    public function getRepos()
    {
        return $this->getUrl(
            'https://api.github.com/users/' . $this->username . '/repos?page=' . $this->page
        );
    }

    /**
     * Make GET request according url and return
     * reponse array.  
     * @param string $url
     * @return array
     */
    private function getUrl($url)
    {    
        $res = (new \GuzzleHttp\Client())->request('GET', $url, ['auth' => $this->githubAuth]);
        return json_decode($res->getBody(), true);
    }
}
