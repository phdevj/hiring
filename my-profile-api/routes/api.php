<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Configure end-points
 * 
 * @author Paulo Henrique <paullo_h@hotmail.com>
 */
Route::middleware(['cors'])->group(function () {
    Route::get('github-users/{username}', 'GithubUsersController@index');
    
    Route::get('github-users/{username}/followers/page/{page}', 'GithubUsersController@findFollowers');
    Route::get('github-users/{username}/following/page/{page}', 'GithubUsersController@findFollowing');
    Route::get('github-users/{username}/repos/page/{page}', 'GithubUsersController@findRepos');

    Route::get('github-users/{username}/orgs', 'GithubUsersController@findOrgs');
});
