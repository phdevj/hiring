export class Org {
    //Keys    
    id: number;
    login: string;

    //About
    description: string;

    //URLs
    url: string;
    avatar_url: string;

}
