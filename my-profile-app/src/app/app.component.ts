import { Component } from '@angular/core';
import { User } from './user/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  username: string;

  showUser = false;
  hideSearch = false;
  userNotFound = false;

  findUser(): void {
    this.showUser = true;
    this.userNotFound = false;
  }
  
  checkUserIsLoaded(reponse: User|boolean|null): void {    
    if (reponse) {
      this.hideSearch = true;
    }
    if (reponse === false) {
      this.hideSearch = false;
      this.userNotFound = true;
      this.showUser = false;
    }
    if (reponse === null) {
      this.hideSearch = false;      
      this.showUser = false;
    }
  }
}
