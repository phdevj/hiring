export class Repo {
    //Keys
    id: number;

    //About
    name: string;    
    description: string;        
    private: boolean;    

    //Numbers
    stargazers_count: number;
    watchers_count: number;
    forks_count: number;

    //URLs
    html_url: string;    
}
