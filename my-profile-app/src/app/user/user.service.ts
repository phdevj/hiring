import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import { User, Follows } from './user';
import { Org } from '../org/org';
import { Repo } from '../repo/repo';

@Injectable()
export class UserService {

  constructor(private http: Http) { }

  /*
  * Get Github User by username
  * @username Github username
  */
  getUser(username: string): Promise<User> {
    const url = `http://localhost:8080/api/github-users/${ username }`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as User)
      .catch(this.handleError)
  }

  /*
  * Get all followers of a user
  * @username Github username
  * @page page number
  */
  getFollowers(username: string, page: number): Promise<Follows[]> {
    const url = `http://localhost:8080/api/github-users/${ username }/followers/page/${ page }`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as Follows[])
      .catch(this.handleError)
  }

  /*
  * Get all following users
  * @username Github username
  * @page page number
  */
  getFollowing(username: string, page: number): Promise<Follows[]> {
    const url = `http://localhost:8080/api/github-users/${ username }/following/page/${ page }`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as Follows[])
      .catch(this.handleError)
  }
  
  /*
  * Get all orgs of a user
  * @username Github username  
  */
  getOrgs(username: string): Promise<Org[]> {
    const url = `http://localhost:8080/api/github-users/${ username }/orgs`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as Org[])
      .catch(this.handleError)
  }

  /*
  * Get all repositories of a user
  * @username Github username
  * @page page number
  */
  getRepos(username: string, page: number): Promise<Repo[]> {
    const url = `http://localhost:8080/api/github-users/${ username }/repos/page/${ page }`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as Repo[])
      .catch(this.handleError)
  }

  private handleError(error: any): Promise<any> {    
    return Promise.reject(error.message || error);
  }
}
