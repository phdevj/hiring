import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ViewEncapsulation } from '@angular/core'
import { User } from './user';
import { UserService } from './user.service';

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  @Input()
  username: string;

  @Output()
  userLoaded: EventEmitter<User|boolean|null> = new EventEmitter<User|boolean|null>();

  user: User;

  /*
  * Control page numbers
  */
  followersPage = 1;
  followingPage = 1;  
  reposPage = 1;

  FOLLOWERS = 'flw';
  FOLLOWING = 'flwing';  
  REPOS = 'repos';

  constructor(private _userService: UserService) { }
  
  /*
  * Get user on component init
  */
  ngOnInit() {
    this.getUser();    
  }

  /*
  * Clean current user
  */
  cleanUser(): void {
    this.userLoaded.emit(null);
  }

  nextPage(key: string): void {
    switch(key) {
      case this.FOLLOWERS:
        this.followersPage++;
        this.getFollowers();
        break;
      case this.FOLLOWING:
        this.followingPage++;
        this.getFollowing();
        break;
      default:
        this.reposPage++;
        this.getRepos();
        break;      
    }
  }

  previousPage(key: string): void {
    switch(key) {
      case this.FOLLOWERS:
        this.followersPage--;
        this.getFollowers();
        break;
      case this.FOLLOWING:
        this.followingPage--;
        this.getFollowing();
        break;
      default:
        this.reposPage--;
        this.getRepos();
        break;      
    }
  }

  /*
  * Get user by username
  */
  getUser(): void {
    this._userService.getUser(this.username)
      .then((user) => {
        this.user = user;
        this.userLoaded.emit(this.user);
        this.getFollowers();
        this.getFollowing();
        this.getOrgs();
        this.getRepos();
      })
      .catch((error) => {
        this.userLoaded.emit(false);
      });
  }

  /*
  * Get all followers of current user  
  */
  getFollowers(): void {
    this.followersPage = this.followersPage < 1 ? 1 : this.followersPage;
    this._userService.getFollowers(this.username, this.followersPage)
      .then(followers => this.user.follower_list = followers);      
  }

  /*
  * Get all following users  
  */
  getFollowing(): void {
    this.followingPage = this.followingPage < 1 ? 1 : this.followingPage;
    this._userService.getFollowing(this.username, this.followingPage)
      .then(following => this.user.following_list = following);
  }

  /*
  * Get all orgs of current user
  */
  getOrgs(): void {    
    this._userService.getOrgs(this.username)
      .then(orgs => this.user.org_list = orgs);      
  }

  /*
  * Get all repositories of current user  
  */
  getRepos(): void {
    this.reposPage = this.reposPage < 1 ? 1 : this.reposPage;
    this._userService.getRepos(this.username, this.reposPage)
      .then(repos => this.user.repo_list = repos);
  }
}
