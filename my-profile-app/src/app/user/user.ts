import { Org } from '../org/org';
import { Repo } from '../repo/repo';

export class Person {
    //Keys
    id: number;
    login: string;   

    //URLS
    avatar_url: string;
    html_url: string;
}

export class Follows extends Person { }

export class User extends Person{    
     
    //About
    name: string;
    company: string;
    blog: string;
    location: string;
    email: string;
    hireable: any;
    bio: string;

    //Numbers
    public_repos: number;
    public_gists: number;    
    followers: number;
    following: number;

    follower_list: Follows[];
    following_list: Follows[];
    org_list: Org[];
    repo_list: Repo[];
}
