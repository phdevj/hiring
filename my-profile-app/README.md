# MyProfileApp

## REQUIREMENTS
------------

- NPM >= 5.5.*
- Angular CLI >= 1.5.3

## Development server

Run `npm start` to install dependencies

## Development server

Run `ng serve` or `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.